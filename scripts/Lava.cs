using Godot;

public partial class Lava : Area2D
{
	[Export] private float _speed = -40.0f;
	[Export] private float _acceleration = -10.0f;

	public override void _Process(double delta){
		Vector2 position = Position;

		position.Y += _speed * (float)delta;

		Position = position;

		_speed += _acceleration * (float)delta;
	}

	private void OnObjectEnter(Node2D body){
		body.QueueFree();
	}
}
