using Godot;

public partial class Gameplay : Node2D
{
	[Export] private PackedScene _floor;
	[Export] private uint _floorCount;
	[Export] private float _floorMaxGap;
	[Export] private float _floorMinGap;
	[Export] private float _floorBoundLeft;
	[Export] private float _floorBoundRight;

	[Export] private Control _gameoverMenu;
	private Player _player;
	private Timer _counterTimer;
	private RichTextLabel _counterText;
	private float _floorHeight = 0.0f;

	public override void _EnterTree(){
		GetTree().Paused = false;

		_player = GetNode<Player>("Player");
		_counterTimer = GetNode<Timer>("CounterTimer");
		_counterText = GetNode<RichTextLabel>("HUD/CounterText");

		_counterTimer.Timeout += CycleGame;

		if(Floor.count < _floorCount){
			_floorHeight = _player.Position.Y;
			for(int i=0; i < _floorCount; i++){
				Floor floor = _floor.Instantiate<Floor>();
				
				floor.Position = new Vector2((float)GD.RandRange(_floorBoundLeft, _floorBoundRight), (float)GD.RandRange(_floorHeight - _floorMaxGap, _floorHeight - _floorMinGap));
				_floorHeight = floor.Position.Y;

				AddChild(floor);
			}
		}
	}

	public override void _Process(double delta) {
		_counterText.Text = "" + (int)_counterTimer.TimeLeft;
	}

	private void PlayerDeath(){
		_counterTimer.Stop();

		_gameoverMenu.Visible = true;

		Tween tween = GetTree().CreateTween();
		tween.TweenProperty(_gameoverMenu, "modulate", Colors.White, 0.45f)
			.From(Colors.Transparent)
			.SetTrans(Tween.TransitionType.Expo);
	}

	private void CycleGame(){
		GetTree().Paused = !GetTree().Paused;

		if(Floor.count < _floorCount){
			uint currentFloorCount = _floorCount - Floor.count;

			for(int i=0; i < currentFloorCount; i++){
				Floor floor = _floor.Instantiate<Floor>();
				
				floor.Position = new Vector2((float)GD.RandRange(_floorBoundLeft, _floorBoundRight), (float)GD.RandRange(_floorHeight - _floorMaxGap, _floorHeight - _floorMinGap));
				_floorHeight = floor.Position.Y;

				AddChild(floor);
			}
		}
	}

	private void OnQuitButtonPressed(){
		GetTree().Quit();
	}
}
