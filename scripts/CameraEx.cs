using Godot;

public partial class CameraEx : Camera2D
{
	[Export] private Node2D target;

	public override void _Process(double delta){
		Position = target.Position;
	}
}
