using Godot;

public partial class Player : CharacterBody2D
{
	[ExportGroup("Movement")]
	[Export] private float _moveSpeed = 300.0f;
	[Export] private float _jumpVelocity = -1000.0f;
	[ExportCategory("Coyote Time")]
	[Export] private float _inputBuffer = 0.05f;
	[Export] private float _groundBuffer = 0.05f;

	[Signal] public delegate void DeathEventHandler();

	private float _gravity = ProjectSettings.GetSetting("physics/2d/default_gravity").AsSingle() * 2.0f;
	private float _inputTimer = 0.0f;
	private float _groundTimer = 0.0f;
	private AudioStreamPlayer2D _audioPlayer;
	private AnimatedSprite2D _sprite;
	private AnimationPlayer _animPlayer;

	public override void _Ready(){
		_sprite = GetNode<AnimatedSprite2D>("AnimatedSprite2D");
		_animPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
		_audioPlayer = GetNode<AudioStreamPlayer2D>("AudioStreamPlayer2D");
	}

	public override void _PhysicsProcess(double delta)
	{
		Vector2 velocity = Velocity;

		// Coyote Timer
		_inputTimer -= (float)delta;
		_groundTimer -= (float)delta;

		// Set Gravity
		if (!IsOnFloor())
			velocity.Y += _gravity * (float)delta;

		// Jump
		if(IsOnFloor())
			_groundTimer = _groundBuffer;
		if(Input.IsActionJustPressed("jump"))
			_inputTimer = _inputBuffer;

		if(_inputTimer > 0 && _groundTimer > 0){
			velocity.Y = _jumpVelocity;
			_audioPlayer.Play();

			_inputTimer = 0.0f;
			_groundTimer = 0.0f;
		}

		// Move
		float direction = Input.GetAxis("move_left", "move_right");
		if(direction != 0){
			velocity.X = Mathf.Lerp(velocity.X, direction * _moveSpeed, 0.2f);
			if(direction == 1){
				_sprite.Play("look_right");
			} else if(direction == -1){
				_sprite.Play("look_left");
			}
		} else {
			velocity.X = Mathf.Lerp(velocity.X, 0, 0.2f);
			_sprite.Play("blink");
		}

		if(velocity.Y < 0){
			_animPlayer.Play("jump");
		} else if(velocity.Y > 0){
			_animPlayer.Play("fall");
		} else {
			_animPlayer.Play("none");
		}

		// Set Velocity
		Velocity = velocity;
		MoveAndSlide();

		// Clamp position border
		Vector2 position = Position;
		position.X = Mathf.Clamp(position.X, -370, 350);

		Position = position;
	}

	public override void _ExitTree(){
		EmitSignal("Death");
	}

	private void ObstacleOutOfView(Node2D body){
		body.QueueFree();
	}
}
