using Godot;

public partial class Main : Node
{
	[Export] private PackedScene _startMenu;
	[Export] private PackedScene _gameplay;

	private void Play(){
		GetNode<CanvasLayer>("StartMenu").QueueFree();
		Gameplay gameplay = _gameplay.Instantiate<Gameplay>();

		AddChild(gameplay);
	}
}
