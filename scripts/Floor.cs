using Godot;

public partial class Floor : StaticBody2D
{
	public static uint count = 0;

	public override void _Ready(){
		count += 1;
	}

	public override void _ExitTree(){
		count -= 1;
	}
}
