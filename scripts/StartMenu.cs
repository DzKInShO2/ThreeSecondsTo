using Godot;

public partial class StartMenu : CanvasLayer
{
	[Signal] public delegate void PlayEventHandler();

	private void OnPlayButtonPrassed(){
		EmitSignal("Play");
	}

	private void OnQuitButtonPressed(){
		GetTree().Quit();
	}
}
